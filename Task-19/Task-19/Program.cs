﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-19 - Check values of an Array and put them in a list

            var numbers = new int[7] { 34, 45, 21, 44, 67, 88, 86 };
            var count = 7;
            var even = new List<int>();
            var i = 0;
            var odd = new List<int>();

            for (i = 0; i < count; i++)
            {
                if (numbers[i] % 2 == 0)
                {
                    even.Add(numbers[i]);
                }
                else
                {
                    odd.Add(numbers[i]);
                }
            }

            Console.WriteLine($"Even numbers: {string.Join(", ", even)}");
            Console.WriteLine($"Odd numbers: {string.Join(", ", odd)}");
        }
    }
}
