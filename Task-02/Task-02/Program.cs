﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task-02-Grant Wheeler, get user input for the month and day they were born and display in a sentence.

            Console.WriteLine("Please input the month and day of your birth: ");

            string birthday = (Console.ReadLine());

            Console.WriteLine($"Thankyou, the month and day of your birth is: {birthday} ");
        }
    }
}
