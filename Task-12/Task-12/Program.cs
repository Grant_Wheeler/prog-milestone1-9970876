﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-12 - Check if a number is even or odd.
            var num = 0;

            Console.WriteLine("Enter a number: ");
            num = int.Parse(Console.ReadLine());

            if ((num % 2) == 0)
            {
                Console.WriteLine("The number is even.");
            }
            else
            {
                Console.WriteLine("The number is odd.");
            }
        }
    }
}
