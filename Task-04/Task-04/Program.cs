﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task-04-Grant Wheeler, using if statements and/or switches, convert celcius to farenheit or vice-versa.
            var userchoice = "";
            double fa = 0;
            double ce = 0;

            Console.WriteLine("Please enter either \"c\" for celsius or \"f\" for farenheit to start conversion: ");
            userchoice = (Console.ReadLine());
            if (userchoice == "c")
            {
                Console.WriteLine("Enter the temp in celsius you wish to calculate: ");
                ce = double.Parse(Console.ReadLine());
                fa = (ce * 9) / 5 + 32;
                Console.WriteLine($"{ce} degrees celsius is equal to {fa} degrees farenheit");
            }
            else if (userchoice == "f")
            {
                Console.WriteLine("Enter the temp in farenheit you wish to calculate: ");
                fa = double.Parse(Console.ReadLine());
                ce = ((fa - 32) * 5) / 9;
                Console.WriteLine($"{fa} degrees farenheit is equal to {ce} degrees celsius");
            }


        }
    }
}
