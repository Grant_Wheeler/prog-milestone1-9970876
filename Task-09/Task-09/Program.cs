﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-09 - Write a program that shows which years are leap years for the next 20 years

            var count = 20;
            var currentYear = 0;
            var i = 0;

            Console.WriteLine("Enter the current year: ");

            currentYear = int.Parse(Console.ReadLine());

            for (i = 0; i <= count; i++)
            {
                if ((currentYear % 4 == 0) && (currentYear % 100 != 0) && (currentYear % 400 != 0))
                    Console.WriteLine($"{currentYear} is a leap year");
                currentYear++;
            }
        }
    }
}
