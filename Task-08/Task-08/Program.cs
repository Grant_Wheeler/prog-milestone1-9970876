﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_08
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-08 - Countdown from 99 to 0 using no more than 5 lines of code.

            for (int i = 0; i <= 99; i++)
                Console.WriteLine(i);
        }
    }
}
