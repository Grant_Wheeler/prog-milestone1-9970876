﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-37 - Calculate how many hours per week you should study for 15 credit course.

            double credits = 15;

            double classHours = 5;

            double semester = 12;

            double studyHours = 10;

            double total = studyHours * credits;
            double totalClassHours = semester * classHours;
            double ownTime = total - totalClassHours;
            double weekly = ownTime / semester;

            Console.WriteLine("Calculating how many hours of study are needed...");
            Console.WriteLine($"{studyHours} hours study is needed for each credit in the course, therefore a total of {total} hours is needed.");
            Console.WriteLine($"With {classHours} hours for lectures and tutorials each week and {semester} weeks in a semester, you will be spending {totalClassHours} hours in class.");
            Console.WriteLine($"{ownTime} hours will be needed for study out of class.");
            Console.WriteLine($"Overall about {weekly} hours will be needed each week.");

            Console.WriteLine("");

            
             
            
        }
    }
}
