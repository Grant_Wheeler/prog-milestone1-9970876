﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-14 - Calculates the computer storage space

            double TB = 0;
            double GB = 0;
            double x = 1024;

            Console.WriteLine("Input the size in TB you wish to convert to GB: ");
            TB = double.Parse(Console.ReadLine());

            GB = TB * x;

            Console.WriteLine($"The size of {TB}TB is equal to {GB}GB");
        }
    }
}