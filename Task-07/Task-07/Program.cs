﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_07
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-07 - Show times table from 1 x to 12 x for given number.

            Console.WriteLine("please enter a number to multiply");
            int num = int.Parse(Console.ReadLine());

            for (int i = 1; i <= 12; i++)
                Console.WriteLine($"{i} * {num} = {i * num}");

        }
    }
}
