﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_27
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-27 - Create an array with colors and print in ascending order

            var colors = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };

            Array.Reverse(colors);

            Console.WriteLine(string.Join(", ", colors));
        }
    }
}
