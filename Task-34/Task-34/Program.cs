﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_34
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-34 - Program that tells you how many working days in number of weeks input by user.

            double weeks = 0;
            double workingDays = 0;

            Console.WriteLine("Please enter the number of weeks you wish to calculate: ");
            weeks = int.Parse(Console.ReadLine());
            workingDays = weeks * 5;

            Console.WriteLine($"The number of working days in {weeks} weeks is: {workingDays} days.");

        }
    }
}
