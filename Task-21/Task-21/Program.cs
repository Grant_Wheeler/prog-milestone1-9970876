﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-21 - Create a dictionary with strings and int

            var dictionary = new Dictionary<string, string>();
            var months = new List<string>();
            

            dictionary.Add("January", "31");
            dictionary.Add("Febuary", "29");
            dictionary.Add("March", "31");
            dictionary.Add("April", "30");
            dictionary.Add("May", "31");
            dictionary.Add("June", "30");
            dictionary.Add("July", "31");
            dictionary.Add("August", "31");
            dictionary.Add("September", "30");
            dictionary.Add("October", "31");
            dictionary.Add("November", "30");
            dictionary.Add("December", "31");

            foreach (var x in dictionary)
            {
                if (x.Value == "31")
                {
                    months.Add(x.Key);
                }
            }

            Console.WriteLine($"{months.Count} months, {string.Join(", ", months)} have 31 days in them.");
        }
    }
}
