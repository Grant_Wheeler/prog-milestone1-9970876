﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-25 - Program that takes user input for a number but cannot crash

            int num;
            do
            {
                Console.WriteLine("Please input a number! ");
            } while (!int.TryParse(Console.ReadLine(), out num));

            Console.WriteLine("You entered {0}", num);
        }
    }
}
