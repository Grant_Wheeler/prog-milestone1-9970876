﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-40 - How many wednesdays are in a month.

            var wednesdays = 0;
            var days = 7;
            var weeks = 4;

            wednesdays = (weeks * days) / days;
            var month = int.Parse(wednesdays.ToString());

            Console.WriteLine($"There are {wednesdays} mondays in this month");
        }
    }
}
