﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-35 - Ask for 5 different math questions based on 3 numbers the user inputs.

            var num1 = 0;
            var num2 = 0;
            var num3 = 0;

            Console.WriteLine("Please enter 3 numbers you wish to calculate");

            Console.WriteLine("Enter the 1st number: ");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the 2nd number: ");
            num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the 3rd number: ");
            num3 = int.Parse(Console.ReadLine());

            Console.WriteLine($"{num1} + {num2} + {num3} = {num1 + num2 + num3}");
            Console.WriteLine($"{num1} - {num2} - {num3} = {num1 - num2 - num3}");
            Console.WriteLine($"{num1} * {num2} * {num3} = {num1 * num2 * num3}");
            Console.WriteLine($"{num1} + {num2} * {num3} = {num1 + num2 * num3}");
            Console.WriteLine($"{num1} / {num2} / {num3} = {num1 / num2 / num3}");
        }
    }
}
