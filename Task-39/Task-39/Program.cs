﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_39
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-39 - How many mondays are in a month.

            var mondays = 0;
            var days = 7;
            var weeks = 4;

            mondays = (weeks * days) / days;
            var month = int.Parse(mondays.ToString());
            
            Console.WriteLine($"There are {mondays} mondays in this month");
        }
    }
}
