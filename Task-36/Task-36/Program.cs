﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            //Grant Wheeler Task-19 - Check values of an Array and put them in a list

            var count = 10;
            var i = 0;

            for (i = 0; i < count; i++)
            {
                if (count == i)
                {
                    Console.WriteLine($"The count {count} is equal to the index {i}");
                }
                else
                {
                    Console.WriteLine($"The count {count} is not equal to the index {i}");
                }
            }
    }
}
